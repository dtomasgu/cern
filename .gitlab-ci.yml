stages:
  - build
  - deploy
  - cleanup

before_script:
  - mkdir -p .repo
  - export CLUSTER_NAME="cci-deleteme-helm-cern-$(echo $CI_COMMIT_SHA | head -c 6)"
  - export CLUSTER_TEMPLATE="kubernetes-1.18.6-3"
  - export OS_AUTH_URL="https://keystone.cern.ch/v3"
  - export OS_IDENTITY_API_VERSION="3"
  - export OS_IDENTITY_PROVIDER="sssd"
  - export OS_INTERFACE="public"
  - export OS_MUTUAL_AUTH="disabled"
  - export OS_PASSWORD="$OS_PASSWORD"
  - export OS_PROJECT_DOMAIN_ID="default"
  - export OS_PROJECT_NAME="$OS_PROJECT_NAME"
  - export OS_USERNAME="$OS_USERNAME"
  - export OS_REGION_NAME="cern"
  - export OS_VOLUME_API_VERSION="2"

build:
  stage: build
  image: gitlab-registry.cern.ch/cloud/ciadm:v0.3.9
  script:
    - curl -o helm.tar.gz https://kubernetes-helm.storage.googleapis.com/helm-v2.14.1-linux-amd64.tar.gz; mkdir -p helm; tar zxvf helm.tar.gz -C helm; cp helm/linux-amd64/helm /usr/local/bin; rm -rf helm*
    - helm init --client-only --stable-repo-url=https://charts.helm.sh/stable
    - for chart in $(ls -d */Chart.yaml | xargs dirname); do helm dep update ${chart}; helm lint ${chart}; helm package ${chart}; done
  except:
    - tags

version-check:
  stage: build
  image: gitlab-registry.cern.ch/cloud/ciadm:v0.3.9
  script:
    - |
        set -x
        CHART=$(git diff-tree --name-only --no-commit-id HEAD origin/master)
        for base in $(find . -maxdepth 1 -type f | sed 's|./||' | xargs); do
            CHART=$(echo ${CHART} | sed "s|${base}||")
        done
        if [ "$(echo ${CHART} | wc -w)" = 0 ]; then
            echo "Changes do not affect charts. skipping"
            exit 0;
        elif [ "$(echo ${CHART} | wc -w)" != 1 ]; then
            echo "ERROR: You can only merge changes on one chart. Please fix before merging again."
            exit 1;
        else
            VDIFF="$(echo "$(git diff origin/master -- $CHART/Chart.yaml)" | grep "\-version:" || true)"
            if [ "${VDIFF}" == "" ]; then
                echo "${CHART} is a new chart, not checking version bump"
                exit 0;
            fi
            OLD_CHART_VERSION="$(echo "${VDIFF}" | awk '{print $2}')"
            # Check and accept if it's a new chart
            if [ "${OLD_CHART_VERSION}" == "" ]; then
                echo "${CHART} is a new chart, not checking version bump"
                exit 0;
            fi
            NEW_CHART_VERSION="$(echo "$(git diff origin/master -- $CHART/Chart.yaml)" | grep "+version:" | awk '{print $2}')"
        fi
    - |
        if [ ${NEW_CHART_VERSION} = "" ] || \
            [ $(expr ${NEW_CHART_VERSION} \<= ${OLD_CHART_VERSION}) -eq 1 ]; then
            echo "ERROR: Chart version must be higher than existent. Please fix before merging again."
            exit 1
        fi
  except:
    - tags

test:
  stage: build
  image: gitlab-registry.cern.ch/cloud/ciadm:v0.3.9
  script:
    - | 
      set -x
      openstack coe cluster create --cluster-template $CLUSTER_TEMPLATE --node-count 1 --flavor m2.medium --merge-labels --labels eos_enabled=false $CLUSTER_NAME 
      sleep 10
      STATUS=$(openstack coe cluster show $CLUSTER_NAME -c status -f value)
      while [ "${STATUS}" != "CREATE_COMPLETE" ] && [ "${STATUS}" != "CREATE_FAILED" ]
      do
        STATUS=$(openstack coe cluster show $CLUSTER_NAME -c status -f value)
        echo "Current cluster status ... $STATUS $(date)"
        sleep 10
      done

      openstack coe cluster show --fit $CLUSTER_NAME
      openstack coe cluster config $CLUSTER_NAME
      export KUBECONFIG=`pwd`/config
      for chart in eosxd; do # add charts to be tested in here
        helm3 upgrade -i --wait --namespace kube-system eosxd ./eosxd
        helm3 test --logs --namespace kube-system eosxd
      done
  except:
    - tags

deploy:
  stage: deploy
  image: gitlab-registry.cern.ch/cloud/ciadm:v0.3.9
  script:
    - helm init --client-only --stable-repo-url=https://charts.helm.sh/stable
    - helm repo add cern https://registry.cern.ch/chartrepo/cern
    - helm repo update
    # helm-push not possible for now as it lacks --sign to pass a provenance file
    # - helm plugin install https://github.com/chartmuseum/helm-push
    - echo $HARBOR_SIGNKEY | base64 -d > secring.gpg
    - |
        set -x
        for chart in $(ls -d */Chart.yaml | xargs dirname); do
            # Get local and remote versions
            LOCAL_VERSION=$(grep -R version ${chart}/Chart.yaml | awk '{print $2}')
            if ! REMOTE_LATEST_VERSION="$(helm search cern/"${chart}" | grep cern/"${chart}" | awk '{print $2}')" ; then
                echo "INFO There are no remote versions."
                REMOTE_LATEST_VERSION=""
            fi
            # Only push if chart version does not exists in remote
            if [ "${REMOTE_LATEST_VERSION}" = "" ] || \
                [ "$(expr "${REMOTE_LATEST_VERSION}" \< "${LOCAL_VERSION}")" -eq 1 ]; then
                helm dep update ${chart}
                helm package --sign --key registry --keyring secring.gpg ${chart}
                set +x
                curl --fail -F "chart=@${chart}-${LOCAL_VERSION}.tgz" -F "prov=@${chart}-${LOCAL_VERSION}.tgz.prov" -H "authorization: Basic $(echo -n ${HARBOR_USER}:${HARBOR_TOKEN} | base64)" https://registry.cern.ch/api/chartrepo/cern/charts
                set -x
            fi
        done
  only:
    - tags

cleanup:
  stage: cleanup
  image: gitlab-registry.cern.ch/cloud/ciadm:v0.3.9
  script: |
    set -x
    STATUS=$(openstack coe cluster show $CLUSTER_NAME -c status -f value)
    while true
    do
      if [ "${STATUS}" != "DELETE_IN_PROGRESS" ]
      then
        openstack coe cluster delete $CLUSTER_NAME || true
      fi
      sleep 10
      if STATUS=$(openstack coe cluster show $CLUSTER_NAME -c status -f value)
      then
        echo "Current cluster status ... $STATUS $(date)"
      else
        echo "done"
        break
      fi
    done
  when: always
  except:
    - tags
